from rest_framework import serializers
from .models import User
from django.contrib.auth.hashers import make_password


class UserSerializer(serializers.ModelSerializer):

    date_joined = serializers.ReadOnlyField()

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get("first_name",
                                                 instance.first_name)
        instance.last_name = validated_data.get("last_name",
                                                instance.last_name)
        instance.email = validated_data.get("email", instance.email)
        instance.description = validated_data.get("description",
                                                  instance.description)
        instance.date_joined = validated_data.get("date_joined",
                                                  instance.date_joined)
        instance.username = validated_data.get("username",
                                               instance.username)
        instance.password = make_password(validated_data.get
                                          ("password", instance.password))
        instance.save()
        return instance

    class Meta(object):
        model = User

        fields = ['id', 'first_name', 'last_name', 'email', 'description',
                  'date_joined', 'username', 'password']
        extra_kwargs = {'password': {'write_only': True}}
