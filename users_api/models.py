from django.contrib.auth.models import (BaseUserManager, AbstractUser)
from django.db import models


class UserManager(BaseUserManager):

    def create_user(self, email=None, username=None, password=None,
                    first_name=None, last_name=None,
                    is_active=True, is_admin=False, **kwargs):
        if not username:
            raise ValueError('Provide unique username')
        if not password:
            raise ValueError('Password is required')
        user_obj = self.model(
            email=self.normalize_email(email)
        )
        user_obj.set_password(password)
        user_obj.username = username
        user_obj.email = email
        user_obj.first_name = first_name
        user_obj.last_name = last_name
        user_obj.admin = is_admin
        user_obj.active = is_active
        user_obj.save(using=self._db)
        return user_obj

    def create_superuser(self, username=None, email=None, password=None):
        user = self.create_user(username=username, password=password,
                                email=email, is_admin=True)
        return user


class User(AbstractUser):
    description = models.TextField(max_length=250, null=True)
    objects = UserManager()
