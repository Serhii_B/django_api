from rest_framework.decorators import (api_view, permission_classes,
                                       authentication_classes)
from rest_framework.permissions import (IsAdminUser, AllowAny, IsAuthenticated)
from rest_framework.authentication import (BasicAuthentication,
                                           SessionAuthentication)
from rest_framework import status
from rest_framework.response import Response
from .models import User
from .serializers import UserSerializer


def get_user_by_id_method(id):
    """
      Function accepts id and returns user with that id
      :return: dict object and status code
      :rtype: json
    """
    user = User.objects.get(pk=id)
    serializer = UserSerializer(user)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def get_user_by_id(request):
    """
     Function calls another method and passes id of a user that is logged in
     :return: function
     :rtype: @param json
     """
    return get_user_by_id_method(request.user.id)


@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAdminUser])
def get_any_user_for_admin(request, id):
    """
    Function calls another method and passes id of a user taken from url
    :return: function
    :rtype: @param json
    """
    return get_user_by_id_method(id)


@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAdminUser])
def get_all_users_for_admin(request):
    """
     Function will return the list of all
    users including admin users
    :return: json data and status code
    :rtype:  json
    """
    user_list = User.objects.all()
    serializer = UserSerializer(user_list, many=True)
    if serializer:
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def get_users(request):
    """
    Function returns list of users
    :return: list of objects and status code
    :rtype: json
    """
    users = User.objects.filter(is_superuser=False)
    serializer = UserSerializer(users, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([AllowAny])
def create_user(request):
    """
      Function creates user by first_name, last_name, username, email,
       password, description
      :return: created user object and http status
      :rtype: json
      """
    content = {
        "first_name": request.data['first_name'],
        "username": request.data['username'],
        "last_name": request.data['last_name'],
        "email": request.data['email'],
        "description": request.data['description'],
        "password": request.data['password']
    }
    user = UserSerializer(data=content)
    if user.is_valid():
        user.save()
        return Response(content, status=status.HTTP_201_CREATED)
    return Response(status=status.HTTP_400_BAD_REQUEST)


def update_user(id, user_data):
    """
    Function accepts id and json data
    and returns updated user data with status code
    :return: dict object and status code
    :rtype: json
    """
    user = User.objects.get(pk=id)
    serializer = UserSerializer(user, data=user_data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAdminUser])
def update_user_by_admin(request, id):
    """
    Function calls another method and passes id
    from the url and json data from request
    :return: function
    :rtype: @param json
    """
    return update_user(id, request.data)


@api_view(['PUT'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def update_self_user(request):
    """
    Function calls another method and passes id
    of current logged in admin user and json data from request
    :return: function
    :rtype: @param json
    """
    return update_user(request.user.id, request.data)


def delete_user(id):
    """
    Function accepts id and delete user with that id
    and returns status code
    :return: status code
    :rtype: json
    """
    try:
        user = User.objects.get(pk=id)
        user.delete()
        return Response("User was deleted", status=status.HTTP_200_OK)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['DELETE'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def delete_user_by_user(request):
    """
    Function calls another method and passes id
    of current logged in user
    :return: function
    :rtype: @param json
    """
    return delete_user(request.user.id)


@api_view(['DELETE'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAdminUser])
def delete_user_by_admin(request, id):
    """
    Function calls another method and passes id from url
    :return: function
    :rtype: @param json
    """
    return delete_user(id)
