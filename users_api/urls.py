from django.urls import path, include
from users_api import views
urlpatterns = [
    path('my_user/', views.get_user_by_id),
    path('user/<int:id>/', views.get_any_user_for_admin),
    path('show_all/', views.get_users),
    path('users/', views.get_all_users_for_admin),
    path('sign_up/', views.create_user),
    path('update_user/<int:id>/', views.update_user_by_admin),
    path('update_user/', views.update_self_user),
    path('delete_user/<int:id>/', views.delete_user_by_admin),
    path('delete_user/', views.delete_user_by_user)
]
